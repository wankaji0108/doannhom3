﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LTrinhWebNhom3.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int CommentID { get; set; }
        public int PortfolioID { get; set; }
        public Portfolio Portfolio { get; set; }

        

    }
}
