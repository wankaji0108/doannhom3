﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations.Schema;

namespace LTrinhWebNhom3.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public int EmployeeID { get; set; }

        // Thuộc tính để lưu ID từ bảng AspNetUsers
        public string UserId { get; set; }

        // Mối quan hệ khóa ngoại với AspNetUsers
        [ForeignKey("UserId")]
        [ValidateNever]
        public ApplicationUser ApplicationUser { get; set; }


    }
}
