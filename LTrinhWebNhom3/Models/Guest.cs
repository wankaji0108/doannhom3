﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations.Schema;

namespace LTrinhWebNhom3.Models
{
    public class Guest
    {
        public int Id { get; set; }
        public int GuestID { get; set; }
        public string Company { get; set; }


        public string UserId { get; set; }
       

        // Mối quan hệ khóa ngoại với AspNetUsers
        [ForeignKey("UserId")]
        [ValidateNever]
        public ApplicationUser ApplicationUser { get; set; }

    }
}
