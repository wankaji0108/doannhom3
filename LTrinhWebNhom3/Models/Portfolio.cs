﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LTrinhWebNhom3.Models
{
    public class Portfolio
    {
        public int Id { get; set; }
        public int GuestID { get; set; }
        public int PortfolioID { get; set; }     
        public string TagID { get; set; }

        

        public Guest Guest { get; set; }
        public Tag Tag { get; set; }
    }
}
