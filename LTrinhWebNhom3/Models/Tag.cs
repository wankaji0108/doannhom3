﻿using System.ComponentModel.DataAnnotations;

namespace LTrinhWebNhom3.Models
{
    public class Tag
    {
        public int Id { get; set; }

        public int PortfolioID { get; set; }
        public string TagID { get; set; }
        [Display(Name = "Tag Name")]
        
        public string TagName { get; set; }
        public string Description { get; set; }
    }
}
